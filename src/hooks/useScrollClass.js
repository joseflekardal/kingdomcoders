import { useEffect, useRef } from 'react'

function useScrollClass (className = 'active') {
  const ref = useRef()

  useEffect(() => {
    function scrollHandler (event) {
      if (!ref) return

      if (window.scrollY > ref.current.offsetTop - window.innerHeight) {
        ref.current.classList.add(className)
        window.removeEventListener('scroll', scrollHandler)
      }
    }

    window.addEventListener('scroll', scrollHandler)
    scrollHandler()

    return () => {
      window.removeEventListener('scroll', scrollHandler)
    }
  })

  return ref
}

export default useScrollClass
