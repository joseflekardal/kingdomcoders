import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import Img from 'gatsby-image'

import useScrollClass from '../../hooks/useScrollClass'

const WhyWeAreDifferent = () => {
  const ref = useScrollClass('active')

  const data = useStaticQuery(graphql`
    query {
      file(relativePath: { eq: "awmleer.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 400) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)
  
  return (
    <section className="why-we-are-different" ref={ref}>
      <div className="flex">
        <div>
          <Img fluid={data.file.childImageSharp.fluid} />
        </div>
        <div>
          <h2>WHY WE ARE DIFFERENT</h2>
          <p>There is a lot of christian developers that do some different personal projects	but they usually end up as small projects that seldom get spread.</p>
          <p>By connecting to other developers as well as with mission organisations the projects can get bigger and can serve an organisation that all ready has the infrastructure needed to make the project successfull.</p>
        </div>
      </div>
    </section>
  )
}

export default WhyWeAreDifferent
