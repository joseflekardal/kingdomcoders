import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import Img from 'gatsby-image'

const Header = () => {
  const data = useStaticQuery(graphql`
    query {
      file(relativePath: { eq: "nicolewolf.jpeg" }) {
        childImageSharp {
          fixed(width: 1400) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)

  return (
    <header>
      <div
        className="primary-block"
        style={{ backgroundImage: 'url(http://kingdomcoders.org/wp-content/uploads/sites/51/2019/11/circle-background-pattern.png)' }}>
        <p>COME AND JOIN THE</p>
        <h1>KINGDOM<span>CODERS</span></h1>
        <p>KingdomCoders is a vision about connecting christian organisations with christian developers. If you are any of those category we would like to get in touch with you.</p>
      </div>
      <div style={{ backgroundImage: `url("${data.file.childImageSharp.fixed.src}")` }}></div>
    </header>
  )
}

export default Header
