import React from 'react'
import useScrollClass from '../../hooks/useScrollClass'

const GeneratingIdeas = () => {
  const ref = useScrollClass('active')

  return (
    <section className="generating-ideas primary-block pt-0" ref={ref}>
      <h2>GENERATING NEW IDEAS<br/>SOLVING BIG PROBLEMS</h2>
      <p>Lets connect the best from the mission organisations and the christian development community do create someting awsome…</p>
    </section>
  )
}

export default GeneratingIdeas
