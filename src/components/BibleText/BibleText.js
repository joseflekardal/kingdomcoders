import React from 'react'
import useScrollClass from '../../hooks/useScrollClass'

const BibleText = () => {
  const ref = useScrollClass('active')

  return (
    <section className="bible-text" ref={ref}>
      <p>Whatever you do, work heartily, as for the Lord and not for men, knowing that from the Lord you will receive the inheritance as your reward. You are serving the Lord Christ.</p>
      <p><i>Colossians 3:23-24</i></p>
    </section>
  )
}
 export default BibleText
