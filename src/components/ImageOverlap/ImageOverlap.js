import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import Img from 'gatsby-image'

import useScrollClass from '../../hooks/useScrollClass'

const ImageOverlap = () => {
  const ref = useScrollClass('active')
  const data = useStaticQuery(graphql`
    query {
      placeholderImage: file(relativePath: { eq: "kobu-agency.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 800) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  return (
    <section className="image-overlap" ref={ref}>
      <Img fluid={data.placeholderImage.childImageSharp.fluid} />
    </section>
  )
}

export default ImageOverlap
