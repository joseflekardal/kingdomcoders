import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import Img from 'gatsby-image'

import useScrollClass from '../../hooks/useScrollClass'

const LatestProjects = () => {
  const ref = useScrollClass('active')

  const data = useStaticQuery(graphql`
    query {
      first: file(relativePath: { eq: "kaleidico.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 300) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      second: file(relativePath: { eq: "bruce-mars.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 300) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      third: file(relativePath: { eq: "fatos.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 300) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  return (
    <section className="latest-project" ref={ref}>
      <h2>OUR LATEST PROJECT</h2>
      <p>We are right now working on apps special made for christian radio station that with AI can help follow-up workers to get the right listeners to following up</p>
      <div className="flex">
        <div>
          <Img fluid={data.first.childImageSharp.fluid} />
        </div>
        <div>
          <Img fluid={data.second.childImageSharp.fluid} />
        </div>
        <div>
          <Img fluid={data.third.childImageSharp.fluid} />
        </div>
      </div>
    </section>
  )
}

export default LatestProjects
