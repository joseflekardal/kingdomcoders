import React from 'react'

const Footer = () => (
  <footer className="primary-block">
    <h2>LET’S START SOMETHING NEW<span>SAY HELLO!</span></h2>
    
    <p>
      Join our channel on slack
      <br/>
      <a href="https://become-a-kingdomcoder.slack.com">https://become-a-kingdomcoder.slack.com</a>
    </p>

    <p>or</p>

    <p>
      Send us an email to
      <br/>
      <a href="mailto:hello@kingdomcoders.org">
        hello@kingdomcoders.org
      </a>
    </p>
  </footer>
)

export default Footer
