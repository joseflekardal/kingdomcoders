import React from 'react'
import { FontAwesomeIcon  } from '@fortawesome/react-fontawesome'
import { faLightbulb, faHeart, faCheckSquare } from '@fortawesome/free-solid-svg-icons'
import useScrollClass from '../../hooks/useScrollClass'

const WhatWeDo = () => {
  const ref = useScrollClass('active')

  return (
    <section className="pb-0 what-we-do" ref={ref}>
      <h2>WHAT WE DO</h2>
        <div className="flex">
          <div>
            <FontAwesomeIcon icon={faLightbulb} color="#fcd21d" size="3x"/>
            <h3>Strategy</h3>
            <p>A lot of the times the organisations that we work with don’t know exactly what they want so every projects begins with a process of understanding the need and come up with the best solution to it.</p>
        </div>

        <div>
          <FontAwesomeIcon icon={faHeart} color="#fcd21d" size="3x" />
          <h3>Development</h3>
          <p>We love create system, apps, websites, software that help organisations get more efficient in what they do.</p>
        </div>

        <div>
          <FontAwesomeIcon icon={faCheckSquare} color="#fcd21d" size="3x" />
          <h3>Maintenance</h3>
          <p>We make sure that there is a strategy of how to mainteance the new app/website/system so that it can live for a long time.</p>
        </div>
      </div>
    </section>
  )
}

export default WhatWeDo
