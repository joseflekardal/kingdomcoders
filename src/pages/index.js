import React from 'react'

import Seo from '../components/Seo'

import Header from '../components/Header'
import WhatWeDo from '../components/WhatWeDo'
import ImageOverlap from '../components/ImageOverlap'
import GeneratingIdeas from '../components/GeneratingIdeas'
import WhyWeAreDifferent from '../components/WhyWeAreDifferent'
import LatestProjects from '../components/LatestProjects'
import BibleText from '../components/BibleText'
import Footer from '../components/Footer'

import '../index.css'

const IndexPage = () => (
  <>
    <Seo title="Home" />
    <Header />

    <WhatWeDo />

    <ImageOverlap />

    <GeneratingIdeas />

    <WhyWeAreDifferent />

    <LatestProjects />

    <BibleText />

    <Footer />
  </>
)

export default IndexPage
