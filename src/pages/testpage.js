import React from "react"

import Seo from '../components/Seo'

import Header from '../components/Header'
import Footer from '../components/Footer'

import '../index.css'

const IndexPage = () => (
  <>
    <Seo title="Test page" />

    <Header />

    <section>
      <h1>This is a test page</h1>
    </section>

    <Footer />
  </>
)

export default IndexPage
